<?php
require_once 'vendor/autoload.php';

use GuzzleHttp\Client;
use GuzzleHttp\Exception\TransferException;

$client = new Client(['base_uri' => 'https://death.star.api']);

try {
    // Get token
    $tokenResponse = $client->request('POST', '/token', [
        'headers' => [
            'Content-Type'     => 'application/x-www-form-urlencoded',
            'Accept'     => 'application/json',
        ],
        'query' => [
            'client_id' => 'Alderan',
            'client_secret' => 'R2D2'
        ],
        'form_params' => [
            'grant_type' => 'client_credentials'
        ]
    ]);

    $tokenBody = json_decode($tokenResponse->getBody());

    // Delete exhaust
    $client->request('DELETE', '/reactor/exhaust/1', [
        'headers' => [
            'Content-Type'     => 'application/json',
            'Authorization'     => sprintf('Bearer %s', $tokenBody['access_token']),
            'x-torpedoes'     => 2,
        ],
    ]);

    // Get prisoner
    $prisonerResponse = $client->request('GET', '/prisoner/leia', [
        'headers' => [
            'Content-Type'     => 'application/json',
            'Authorization'     => sprintf('Bearer %s', $tokenBody['access_token']),
        ],
    ]);
} catch (TransferException $e) {
    echo Psr7\str($e->getRequest());
    echo Psr7\str($e->getResponse());
}